# README #

This repository will contain contents from tutorials

#Saturday, Oct 14 2017
#### File Contents
##### Python
###### Loading CSV and Plotting Charts

Launch Jupyter in Anaconda and Navigate to the location of this folder to open the ipynb file.

1. import.ipynb
2. plot.ipynb
3. other_samples.ipynb
4. class.ipynb

##### VB
1. SampleForm - Contains a sample form for uploading a CSV file into a database and showing the content of the database in a datagridview.
Form has the ability to update, delete a specific row in the database by selecting the row in the datagridview.
It also has the ability of clearing the whole table.  

#Saturday, Oct 21 2017
#### File Contents
##### C#
1. MVC - Contains a sample form containing MVC components.  


#Saturday, Oct 28 2017
#### File Contents
##### HTML With bootstrap
1. Folder containing HTML files with a little bit of bootstrap styling. 
##### MVC With bootstrap
1. Folder containing MVC template files with a little bit of bootstrap styling.
