﻿using Microsoft.EntityFrameworkCore;

namespace MVC2.Models
{
    public class MvcMovieContext : DbContext
    {
        public MvcMovieContext(DbContextOptions<MvcMovieContext> options) : base(options)
        {
        }

        public DbSet<MVC2.Models.Movie> Movie { get; set; }
    }
}
