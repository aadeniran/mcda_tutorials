import matplotlib.pyplot as pt
import numpy as np
import statistics


import matplotlib.mlab as mlab

objects = [4.1, 1.5, 10.4, 5.7, 3.0, 5.9, 3.4]

standard_dev = statistics.stdev(objects)
median = statistics.median(objects)

number_bins = 30

n, bins, patchs = pt.hist(objects, number_bins, normed=1, facecolor='red', alpha=1.0)

y = mlab.normpdf(bins, standard_dev, median)
pt.plot(bins, y, 'r--')
pt.xlabel("Sample")
pt.ylabel("Probability")
pt.show()
