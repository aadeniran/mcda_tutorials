Imports System
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Partial Public Class CustomersModel
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=CustomersModel")
    End Sub

    Public Overridable Property Customers As DbSet(Of Customer)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.FirstName) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.LastName) _
            .IsUnicode(False)
    End Sub
End Class
