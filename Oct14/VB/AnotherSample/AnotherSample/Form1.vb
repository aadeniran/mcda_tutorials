﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadData()
    End Sub

    Private Sub LoadData()
        Using context As New CustomersModel
            Dim listOfCustomers = context.Customers.ToList
            If listOfCustomers.Count > 0 Then
                CSV_Output.Visible = True
                CSV_Output.DataSource = listOfCustomers
            Else
                CSV_Output.Visible = False
            End If
        End Using
    End Sub

    Private Sub ImportCSV()
        Dim csv_path As String = ""
        Dim FD As OpenFileDialog = New OpenFileDialog()
        FD.Title = "Select CSV"
        FD.FilterIndex = 2
        FD.RestoreDirectory = True
        If FD.ShowDialog() = DialogResult.OK Then
            csv_path = FD.FileName
            TextBox1.Text = csv_path
            Button2.Enabled = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ImportCSV()
    End Sub

    Private Sub InsertData(NewDetails As Customer)
        Using context As New CustomersModel
            context.Customers.Add(NewDetails)
            context.SaveChanges()
        End Using
        LoadData()

    End Sub

    Private Sub LoadCSVFile()
        Dim csv_path As String = TextBox1.Text
        Using MyReader As New Microsoft.VisualBasic.
                              FileIO.TextFieldParser(
                                csv_path)
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(",")
            Dim currentRow As String()
            Dim rowNumber As Integer = 0
            Dim firstLine As Boolean = True
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    Dim rowData As New ArrayList
                    Dim currentField As String
                    If firstLine Then
                        firstLine = False
                    Else
                        For Each currentField In currentRow
                            rowData.Add(currentField)
                        Next
                        Dim NewCustomer As New Customer
                        NewCustomer.FirstName = rowData.Item(0)
                        InsertData(NewCustomer)

                    End If
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message & "is not valid and will be skipped.")
                End Try
            End While
        End Using
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        LoadCSVFile()
    End Sub

    Private Sub ClearTable()
        Using context As New CustomersModel
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Customers]")
            context.SaveChanges()
        End Using
        LoadData()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ClearTable()
    End Sub
End Class
