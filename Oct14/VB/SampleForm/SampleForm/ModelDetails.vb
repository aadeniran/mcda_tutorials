Imports System
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Partial Public Class ModelDetails
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=ModelDetails")
    End Sub

    Public Overridable Property ContactInfoes As DbSet(Of ContactInfo)
    Public Overridable Property Details As DbSet(Of Detail)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of ContactInfo)() _
            .Property(Function(e) e.Address) _
            .IsUnicode(False)

        modelBuilder.Entity(Of ContactInfo)() _
            .Property(Function(e) e.PhoneNumber) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Detail)() _
            .Property(Function(e) e.FirstName) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Detail)() _
            .Property(Function(e) e.LastName) _
            .IsUnicode(False)
    End Sub
End Class
