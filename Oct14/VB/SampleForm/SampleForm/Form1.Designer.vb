﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.FilePath = New System.Windows.Forms.TextBox()
        Me.Browse = New System.Windows.Forms.Button()
        Me.Upload = New System.Windows.Forms.Button()
        Me.CSV_content = New System.Windows.Forms.DataGridView()
        Me.clear = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LastName = New System.Windows.Forms.TextBox()
        Me.FirstName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Update = New System.Windows.Forms.Button()
        Me.Delete = New System.Windows.Forms.Button()
        CType(Me.CSV_content, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FilePath
        '
        Me.FilePath.Location = New System.Drawing.Point(12, 12)
        Me.FilePath.Name = "FilePath"
        Me.FilePath.Size = New System.Drawing.Size(397, 20)
        Me.FilePath.TabIndex = 0
        '
        'Browse
        '
        Me.Browse.Location = New System.Drawing.Point(416, 12)
        Me.Browse.Name = "Browse"
        Me.Browse.Size = New System.Drawing.Size(123, 20)
        Me.Browse.TabIndex = 1
        Me.Browse.Text = "Browse"
        Me.Browse.UseVisualStyleBackColor = True
        '
        'Upload
        '
        Me.Upload.Enabled = False
        Me.Upload.Location = New System.Drawing.Point(12, 39)
        Me.Upload.Name = "Upload"
        Me.Upload.Size = New System.Drawing.Size(527, 23)
        Me.Upload.TabIndex = 2
        Me.Upload.Text = "Upload"
        Me.Upload.UseVisualStyleBackColor = True
        '
        'CSV_content
        '
        Me.CSV_content.AllowUserToAddRows = False
        Me.CSV_content.AllowUserToDeleteRows = False
        Me.CSV_content.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CSV_content.Location = New System.Drawing.Point(14, 97)
        Me.CSV_content.Name = "CSV_content"
        Me.CSV_content.ReadOnly = True
        Me.CSV_content.Size = New System.Drawing.Size(526, 110)
        Me.CSV_content.TabIndex = 3
        Me.CSV_content.Visible = False
        '
        'clear
        '
        Me.clear.Location = New System.Drawing.Point(13, 68)
        Me.clear.Name = "clear"
        Me.clear.Size = New System.Drawing.Size(527, 23)
        Me.clear.TabIndex = 4
        Me.clear.Text = "Clear Table"
        Me.clear.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LastName)
        Me.GroupBox1.Controls.Add(Me.FirstName)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Update)
        Me.GroupBox1.Controls.Add(Me.Delete)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 223)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(525, 115)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Single Detail Information"
        Me.GroupBox1.Visible = False
        '
        'LastName
        '
        Me.LastName.Location = New System.Drawing.Point(84, 47)
        Me.LastName.Name = "LastName"
        Me.LastName.Size = New System.Drawing.Size(435, 20)
        Me.LastName.TabIndex = 5
        '
        'FirstName
        '
        Me.FirstName.Location = New System.Drawing.Point(84, 21)
        Me.FirstName.Name = "FirstName"
        Me.FirstName.Size = New System.Drawing.Size(435, 20)
        Me.FirstName.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Last Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "First Name"
        '
        'Update
        '
        Me.Update.Location = New System.Drawing.Point(6, 78)
        Me.Update.Name = "Update"
        Me.Update.Size = New System.Drawing.Size(249, 23)
        Me.Update.TabIndex = 1
        Me.Update.Text = "Update"
        Me.Update.UseVisualStyleBackColor = True
        '
        'Delete
        '
        Me.Delete.Location = New System.Drawing.Point(261, 78)
        Me.Delete.Name = "Delete"
        Me.Delete.Size = New System.Drawing.Size(258, 23)
        Me.Delete.TabIndex = 0
        Me.Delete.Text = "Delete"
        Me.Delete.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 372)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.clear)
        Me.Controls.Add(Me.CSV_content)
        Me.Controls.Add(Me.Upload)
        Me.Controls.Add(Me.Browse)
        Me.Controls.Add(Me.FilePath)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.CSV_content, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FilePath As TextBox
    Friend WithEvents Browse As Button
    Friend WithEvents Upload As Button
    Friend WithEvents CSV_content As DataGridView
    Friend WithEvents clear As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents FirstName As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Update As Button
    Friend WithEvents Delete As Button
    Friend WithEvents LastName As TextBox
End Class
