﻿Public Class Form1

    Dim selectedCell As New Detail
    Private Sub Browse_Click(sender As Object, e As EventArgs) Handles Browse.Click
        ImportCSV()
    End Sub

    ' Get the CSV path from a dialog
    Private Sub ImportCSV()
        Dim importFile As String = ""

        Dim fd As OpenFileDialog = New OpenFileDialog()
        fd.Title = "Select CSV file to import"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            importFile = fd.FileName
            FilePath.Text = importFile
            Upload.Enabled = True
        Else

        End If
    End Sub

    ' Load CSV file and insert into the database
    Private Sub LoadCSVFile(csv_path As String)
        Using MyReader As New Microsoft.VisualBasic.
                              FileIO.TextFieldParser(
                                csv_path)
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(",")
            Dim currentRow As String()
            Dim rowNumber As Integer = 0
            Dim firstLine As Boolean = True
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    Dim rowData As New ArrayList
                    Dim currentField As String
                    If firstLine Then
                        firstLine = False
                    Else
                        For Each currentField In currentRow
                            rowData.Add(currentField)
                        Next
                        Dim NewDetail As New Detail
                        NewDetail.FirstName = rowData.Item(0)
                        NewDetail.LastName = rowData.Item(1)
                        InsertDetails(NewDetail)
                    End If
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message & "is not valid and will be skipped.")
                End Try
            End While
        End Using
        ShowTable()
    End Sub

    'Insert Into Database
    Private Sub InsertDetails(NewDetail As Detail)
        Using context As New ModelDetails
            context.Details.Add(NewDetail)
            context.SaveChanges()
        End Using
    End Sub

    ' Clear the content of the table
    Private Sub ClearTable()
        Using context As New ModelDetails
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Details]")
            context.SaveChanges()
        End Using
        ShowTable()
        GroupBox1.Visible = False
    End Sub

    'Show the content of the table in the datagrid
    Private Sub ShowTable()
        Using context As New ModelDetails
            Dim listOfCustomers = context.Details.ToList
            If listOfCustomers.Count > 0 Then
                CSV_content.Visible = True
                CSV_content.DataSource = listOfCustomers
            Else
                CSV_content.Visible = False
            End If

        End Using

    End Sub

    ' Delete a field in the datatbase
    Private Sub DeleteField(Detail As Detail)
        Using context As New ModelDetails
            context.Details.Attach(Detail)
            context.Details.Remove(Detail)
            context.SaveChanges()
        End Using
        ShowTable()
        GroupBox1.Visible = False
    End Sub

    ' Update a field in the datatbase
    Private Sub UpdateDetail(Detail As Detail)
        Using context As New ModelDetails
            context.Details.Attach(Detail)
            context.Entry(Detail).State = Entity.EntityState.Modified
            context.SaveChanges()
        End Using
        ShowTable()
    End Sub

    Private Sub Upload_Click(sender As Object, e As EventArgs) Handles Upload.Click
        LoadCSVFile(FilePath.Text)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ShowTable()
    End Sub

    Private Sub clear_Click(sender As Object, e As EventArgs) Handles clear.Click
        ClearTable()
    End Sub


    Private Sub Update_Click(sender As Object, e As EventArgs) Handles Update.Click
        Dim updateField As New Detail
        updateField.Id = selectedCell.Id
        updateField.FirstName = FirstName.Text
        updateField.LastName = LastName.Text
        UpdateDetail(updateField)
    End Sub

    Private Sub Delete_Click(sender As Object, e As EventArgs) Handles Delete.Click
        DeleteField(selectedCell)
    End Sub

    Private Sub CSV_content_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles CSV_content.CellClick
        Dim integer_id As Integer
        GroupBox1.Visible = True
        integer_id = CSV_content.CurrentRow.Index
        selectedCell.Id = CSV_content.Item(0, integer_id).Value
        selectedCell.FirstName = CSV_content.Item(1, integer_id).Value
        selectedCell.LastName = CSV_content.Item(2, integer_id).Value
        FirstName.Text = selectedCell.FirstName
        LastName.Text = selectedCell.LastName
    End Sub

End Class
