namespace MVC2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Student> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
                .Property(e => e.First_Name)
                .IsUnicode(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.Last_Name)
                .IsUnicode(false);
        }
    }
}
