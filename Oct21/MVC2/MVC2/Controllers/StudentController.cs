﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC2.Models;

namespace MVC2.Controllers
{
    public class StudentController : Controller
    {
        private Model1 _db = new Model1();
        // GET: Student
        public ActionResult Index()
        {

            return View(_db.Students.ToList());
        }

        public ActionResult Create()
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind(Exclude = "Id")] Student s)
        {
            if (!ModelState.IsValid)
                return View();

            _db.Students.Add(s);
            _db.SaveChanges();

            return View();
        }

        public ActionResult Edit(int id)
        {
            var editStudent = (from stud in _db.Students
                               where stud.Id == id select stud).First();

            return View(editStudent);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Student s)
        {
            var originalStudent = _db.Students.SingleOrDefault(stud => stud.Id == s.Id);
            if (originalStudent != null)
                originalStudent.Last_Name = s.Last_Name;
                originalStudent.First_Name = s.First_Name;
                originalStudent.Score = s.Score;
                 _db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}