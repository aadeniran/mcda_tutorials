namespace Mvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hello")]
    public partial class Hello
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
         public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string helloName { get; set; }

        public int? Age { get; set; }
    }
}
