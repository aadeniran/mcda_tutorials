﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc.Models;
using System.Threading.Tasks;

namespace Mvc.Controllers
{
    public class HelloWorldController : Controller
    {
        private Model1 _db = new Model1();
        List<Hello> helloList = new List<Hello>
            {
                new Hello(){helloName = "test",Age=12},
                new Hello(){helloName = "test2",Age=12},
                new Hello(){helloName = "test3",Age=12}
            };

        // GET: HelloWorld
        public ActionResult Index()
        {
            return View(_db.Helloes.ToList());
        }

        public ActionResult Create()
        {
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind(Exclude ="Id")] Hello hello)
        {
            if (!ModelState.IsValid)
                return View();

            _db.Helloes.Add(hello);
            _db.SaveChanges();

            return View();
        }

        public ActionResult Edit(int id)
        {
            var editHello = (from m in _db.Helloes
                             where m.Id == id
                             select m).First();
            return View(editHello);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Hello hello)
        {
           // Console.(hello.Id);
            System.Console.Write(hello);
            //  var originalHello = (from m in _db.Helloes
            //                 where m.Id == hello.Id
            //               select m).First();
            var originalHello = _db.Helloes.SingleOrDefault(h => h.Id == hello.Id);
            if(originalHello != null)
                originalHello.helloName = hello.helloName;
                originalHello.Age = hello.Age;
                _db.SaveChanges();

            //if (!ModelState.IsValid)
                // originalHello.helloName = hello.helloName;
                // originalHello.Age = hello.Age;
                //   _db.Helloes.
                 //_db.Entry(hello).State = System.Data.Entity.EntityState.Modified;
               //  _db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var editHello = (from m in _db.Helloes
                             where m.Id == id
                             select m).First();
            return View(editHello);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Hello hello)
        {
            // Console.(hello.Id);
            System.Console.Write(hello);
            //  var originalHello = (from m in _db.Helloes
            //                 where m.Id == hello.Id
            //               select m).First();
            var originalHello = _db.Helloes.SingleOrDefault(h => h.Id == hello.Id);
            if (originalHello != null)
                _db.Helloes.Remove(originalHello);
                _db.SaveChanges();

            //if (!ModelState.IsValid)
            // originalHello.helloName = hello.helloName;
            // originalHello.Age = hello.Age;
            //   _db.Helloes.
            //_db.Entry(hello).State = System.Data.Entity.EntityState.Modified;
            //  _db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}