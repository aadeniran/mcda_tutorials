﻿
Imports MySql.Data.MySqlClient

Module Module1

    Dim conn As New MySqlConnection

    Sub Main()
        Dim DatabaseName As String = "reservation"
        Dim server As String = "localhost"
        Dim userName As String = "root"
        Dim password As String = "password"
        If Not conn Is Nothing Then conn.Close()
        conn.ConnectionString = String.Format("server={0}; user id={1}; password={2}; database={3}; pooling=false", server, userName, password, DatabaseName)
        Try
            conn.Open()
            Console.WriteLine("Connected")
        Catch ex As Exception
            Console.WriteLine("Not Connected")
        End Try
        conn.Close()

    End Sub

End Module
