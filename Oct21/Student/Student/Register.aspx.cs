﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;

namespace Student
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //LabelError.Text = "All fields are required.";
        }

        protected void Register_User(object sender, EventArgs e)
        {
            string name = TextName.Text;
            string city = TextCity.Text;
            string email = TextEmail.Text;
            string studentid = TextID.Text;
            if (Page.IsValid)
            {
                Database myDb = new Database();
                string value = myDb.insertData(name, email, studentid, city);
                LabelError.Text = value;
            }
        }
    }

}