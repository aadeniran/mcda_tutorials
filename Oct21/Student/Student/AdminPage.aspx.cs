﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace Student
{
    public partial class AdminPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LabelUser.Text = "Welcome " + Session["username"].ToString();
            }
            catch(Exception)
            {
                Response.Redirect("Login.aspx", true);
            }
        }

        protected void Get_Users(object sender, EventArgs e)
        {
                Database myDB = new Database();
                object obj = myDB.getAllStudent();
                if (obj is string)
                {
                    LabelError.Text = (string)obj;
                }
                else if (obj is DataTable)
                {
                    DataTable dt = (DataTable)obj;
                    //setting up the html
                    StringBuilder html = new StringBuilder();

                    //beginning of table
                    html.Append("<br/><table class='table table-bordered' border = '1'>");

                    //the header row.
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        html.Append("<th>");
                        html.Append(column.ColumnName);
                        html.Append("</th>");
                    }
                    html.Append("</tr>");

                    //the data rows.
                    foreach (DataRow row in dt.Rows)
                    {
                        html.Append("<tr>");
                        foreach (DataColumn column in dt.Columns)
                        {
                            html.Append("<td>");
                            html.Append(row[column.ColumnName]);
                            html.Append("</td>");
                        }
                        html.Append("</tr>");
                    }

                    //table end.
                    html.Append("</table>");

                    //add the html to Placeholder.
                    PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
                }
            
        }

        protected void Log_Out(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx", true);
        }
    }
}