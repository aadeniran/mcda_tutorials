﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminPage.aspx.cs" Inherits="Student.AdminPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <title></title>
</head>
<body>
    <div class="container text-center" style="padding: 50px 0">
        <div class="row">
            <asp:Label ID="LabelUser" runat="server"></asp:Label>
        </div>
    <form id="form1" runat="server">
    <div class="row">
         <div class="col-md-12">
             
        <asp:Button ID="Button1" runat="server" Text="Get Students" CssClass ="btn btn-lg btn-success" OnClick="Get_Users"/>
             </div>
        <div class="col-md-12">
            <asp:Label ID="LabelError" runat="server"></asp:Label>
        </div>
         <div class="col-md-12">
             <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
             </div>
        </div>
        <div class="col-md-12">
             
        <asp:Button ID="Button2" runat="server" Text="Log out" CssClass ="btn btn-lg btn-primary" OnClick="Log_Out"/>
             </div>
    </form>
        </div>
</body>
</html>
