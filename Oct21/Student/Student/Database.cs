﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using MySql.Data.MySqlClient;

namespace Student
{
    public class Database
    {
        //create the connection string with your database settings.
        private string connection = "Server=localhost;Database=reservation;Uid=root;Pwd=password;";

        //function that return a boolean if the admin login is valid
        public Boolean checkAdmin(string username, string password)
        {
            //to use this import the mysql client on line 7
            MySqlConnection conn = new MySqlConnection(connection);
            //declare a mysql statement for querying the database
            MySqlCommand stmt;
            //open your connection to begin with and ensure you close it as indicated in line 47
            conn.Open();
            try
            {

                stmt = conn.CreateCommand();
                stmt.CommandText = "select * from reservation.adminlogin where username = '" + username + "' and password = '" + password + "'";
                //save your query result in the MySqlDataAdapter
                MySqlDataAdapter adap = new MySqlDataAdapter(stmt);
                //declare a datatable and put all the data from the adap into it
                DataTable dt = new DataTable();
                adap.Fill(dt);
                //if the number of the rows in the datable is more than one then you have an admin, if not, you don't
                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

        }

        //for inserting student data into the database.
        public string insertData(string name, string email, string sid, string city)
        {
            MySqlConnection conn = new MySqlConnection(connection);
            MySqlCommand stmt;
            conn.Open();
            if (checkStudentUser(sid))
            {
                try
                {
                    stmt = conn.CreateCommand();
                    stmt.CommandText = "insert into student_details(student_id, name, email, city) values (@sid, @name, @email, @city)";
                    stmt.Parameters.AddWithValue("@sid", sid);
                    stmt.Parameters.AddWithValue("@name", name);
                    stmt.Parameters.AddWithValue("@email", email);
                    stmt.Parameters.AddWithValue("@city", city);
                    stmt.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    return "Failed";
                }
                finally
                {
                    conn.Close();
                }
                return "<p>Registration successful<br/><a href='../Login.aspx'>Please Log in</a></p>";
            }
            else
            {
                return "<p>User already exists</p>";
            }
            

        }

        public Boolean checkStudentUser(string sid)
        {
            MySqlConnection conn = new MySqlConnection(connection);
            MySqlCommand stmt;
            conn.Open();
            try
            {
                stmt = conn.CreateCommand();
                stmt.CommandText = "select * from student_details where student_id = @sid";
                stmt.Parameters.AddWithValue("@sid", sid);
                MySqlDataAdapter adap = new MySqlDataAdapter(stmt);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

        }

        //Get all the 
        public object getAllStudent()
        {
            MySqlConnection conn = new MySqlConnection(connection);
            MySqlCommand stmt;
            conn.Open();
            try
            {
                stmt = conn.CreateCommand();
                stmt.CommandText = "select * from student_details";
                MySqlDataAdapter adap = new MySqlDataAdapter(stmt);
                DataTable dt = new DataTable();
                adap.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return "Error";
            }
            finally
            {
                conn.Close();
            }

        }
    }
}