﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Student.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style/styleregister.css" />
</head>
<body>
    <div class="container text-center">
        <h3>Simple Student Registration page
        </h3>
        <form id="form1" runat="server">
            <div class="row">

                <div style="margin-bottom: 0px">
                    <h4>Fill in the below form.</h4>
                    <div class="form-group">

                        <div class=" col-sm-12">Name:</div>
                        <div class=" col-sm-12">
                            <div class=" col-sm-4"></div>

                            <div class=" col-sm-4">
                            <asp:TextBox ID="TextName" runat="server" TabIndex="1" CssClass="form-control"></asp:TextBox>
                                </div>
                            <div class="col-md-4"></div>
                        </div>

                        <div class=" col-sm-12">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextName" ErrorMessage="Required Field!!!" Font-Size="Small" ForeColor="Maroon"></asp:RequiredFieldValidator>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class=" col-sm-12">Email: </div>
                         <div class=" col-sm-12">
                            <div class=" col-sm-4"></div>

                            <div class=" col-sm-4">
                            <asp:TextBox ID="TextEmail" runat="server" TabIndex="2" CssClass="form-control"></asp:TextBox>
                                </div>
                             <div class="col-md-4"></div>
                        </div>
                        <div class="col-md-12">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Email not in correct format"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextEmail" ErrorMessage="Required Field!!!" Font-Size="Small" ForeColor="Maroon"></asp:RequiredFieldValidator>
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-sm-12">
                            City:
                        </div>
                        <div class=" col-sm-12">
                            <div class=" col-sm-4"></div>

                            <div class=" col-sm-4">
                            <asp:TextBox ID="TextCity" runat="server" TabIndex="3" CssClass="form-control"></asp:TextBox>
                                </div>
                            <div class=" col-sm-4"></div>
                        </div>

                        <div class=" col-sm-12">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextCity" ErrorMessage="Required Field!!!" Font-Size="Small" ForeColor="Maroon"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-sm-12">
                            Student ID:
                        </div>
                        <div class=" col-sm-12">
                            <div class=" col-sm-4"></div>
                            <div class=" col-sm-4">
                                <asp:TextBox ID="TextID" runat="server" TabIndex="4" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class=" col-sm-4"></div>
                        </div>
                    <div class=" col-sm-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextID" ErrorMessage="Required Field!!!" Font-Size="Small" ForeColor="Maroon"></asp:RequiredFieldValidator>

                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class=" col-sm-12">
                    <asp:Button ID="ButtonSubmit" CssClass="btn btn-lg btn-primary" runat="server" CommandName="submit" Text="Register" OnClick="Register_User" />
                </div>
            </div>
    </div>
    </form>
        <div class="row">
            <asp:Label ID="LabelError" runat="server" ForeColor="Maroon"></asp:Label>
        </div>
        <div class=" col-sm-12" style="margin-top:20px;">
                    <a class="btn btn-lg btn-primary" href="../Default.aspx">Home</a>
                </div>
    </div>
</body>
</html>
