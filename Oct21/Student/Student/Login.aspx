﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Student.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style/styleregister.css" />
    <title>Login</title>
</head>
<body>
    <div class="container text-center">
        
        <form id="form1" runat="server">
            <div class="row">
                <h4>Fill in the below form.</h4>
                <div class="form-group">

                    <div class=" col-sm-12">Username:</div>
                    <div class=" col-sm-12">
                        <div class=" col-sm-4"></div>

                        <div class=" col-sm-4">
                            <asp:TextBox ID="TextUserName" runat="server" TabIndex="1" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                    <div class=" col-sm-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextUserName" ErrorMessage="Required Field!!!" Font-Size="Small" ForeColor="Maroon"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="form-group">

                    <div class=" col-sm-12">Password:</div>
                    <div class=" col-sm-12">
                        <div class=" col-sm-4"></div>

                        <div class=" col-sm-4">
                            <asp:TextBox ID="TextPassword" runat="server" TabIndex="2" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                    <div class=" col-sm-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextPassword" ErrorMessage="Required Field!!!" Font-Size="Small" ForeColor="Maroon"></asp:RequiredFieldValidator>

                    </div>
                </div>

            </div>
            <div class="form-group">
                <div class=" col-sm-12">
            <asp:Label ID="LabelError" runat="server" ForeColor="Maroon"></asp:Label>
                    </div>
        </div>
            <div class="form-group">
                <div class=" col-sm-12">
                    <asp:Button ID="ButtonSubmit" CssClass="btn btn-lg btn-primary" runat="server" CommandName="submit" Text="Login" OnClick="Login_User" />
                </div>
            </div>
                <div class=" col-sm-12" style="margin-top:20px;">
                    <a class="btn btn-lg btn-primary" href="../Default.aspx">Home</a>
                </div>
    
    </form>
        </div>
</body>
</html>
