﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Student
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_User(object sender, EventArgs e)
        {
            string username = TextUserName.Text;
            string password = TextPassword.Text;
            if (Page.IsValid)
            {
                Database myDb = new Database();
                Boolean value = myDb.checkAdmin(username, password);
                if (value)
                {
                    Session["username"] = username;
                    Server.Transfer("AdminPage.aspx", true);
                }
                else
                {

                    LabelError.Text = "Invalid user!!!";
                }
            }
        }
    }

  
}