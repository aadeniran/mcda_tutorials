﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace CSVStudent
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //LabelError.Text = "All fields are required.";
        }

        protected void Save_User(object sender, EventArgs e)
        {
            string name = TextName.Text;
            string city = TextCity.Text;
            string email = TextEmail.Text;
            string studentid = TextID.Text;
            if (Page.IsValid)
            {
                using (StreamWriter writer = new StreamWriter("C:\\Users\\User\\Documents\\CSV\\CustomerData.csv", true)) //// true to append data to the file
                {
                    writer.WriteLine("Name" + "," + "Email" + "," + "City" + "," +
                        "Student_ID");
                    writer.WriteLine(name + "," + city + "," + email + "," + studentid);
                    writer.Close();
                }

                Session["name"] = name;
                Session["email"] = email;
                Session["city"] = city;
                Session["sid"] = studentid;
                Server.Transfer("AdminPage.aspx", true);
                
            }
        }
    }

}