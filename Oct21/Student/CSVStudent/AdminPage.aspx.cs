﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace CSVStudent
{
    public partial class AdminPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LabelResult.Text = "Welcome " + Session["sid"].ToString() + "<br/> Name:" + Session["name"].ToString() + ", Email:" + Session["email"].ToString(); 
            }
            catch(Exception)
            {
                Response.Redirect("Register.aspx", true);
            }
        }

    }
}